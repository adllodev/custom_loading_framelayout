package com.adllo.myapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnFrame = (Button) findViewById(R.id.btnFrame);

        btnFrame.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideFrame(v);
            }
        });

//        btnFrame.setOnClickListener(hideFrame());
    }

    private void click()
    {

    }

    public void hideFrame(View view) {
        LinearLayout linear =  findViewById(R.id.layoutLinear);
        ProgressBar progress = findViewById(R.id.loadingSpinner);

        linear.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);

        Button btnFrame = (Button) findViewById(R.id.btnFrame);
        btnFrame.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                showFrame(v);
            }
        });
    }

    public void showFrame(View view) {
        LinearLayout linear =  findViewById(R.id.layoutLinear);
        ProgressBar progress = findViewById(R.id.loadingSpinner);

        linear.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);

        Button btnFrame = (Button) findViewById(R.id.btnFrame);
        btnFrame.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                hideFrame(v);
            }
        });
    }
}
